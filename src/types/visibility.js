export const VISIBILITY_ALL : 'All'  = 'All';
export const VISIBILITY_ACTIVE : 'Active' = 'Active';
export const VISIBILITY_COMPLETED : 'Completed' = 'Completed';

export const VISIBILITY_FILTERS : Array<string> = [
    VISIBILITY_ALL,
    VISIBILITY_ACTIVE,
    VISIBILITY_COMPLETED
]

export const CHANGE_VISIBILITY_FILTER : 'CHANGE_VISIBILITY_FILTER' = 'CHANGE_VISIBILITY_FILTER';

export type Filter = 
    | typeof VISIBILITY_ALL 
    | typeof VISIBILITY_ACTIVE 
    | typeof VISIBILITY_COMPLETED 

export type ChangeFilter = {
    type: typeof CHANGE_VISIBILITY_FILTER, 
    filter : Filter
}