// @flow
import type { Todo, Todos, TodoAction } from '../types/todos'
import {
    ADD_TODO,
    REMOVE_TODO,
    CHANGE_TODO,
    TOGGLE_TODO,
    TOGGLE_ALL_TODOS,
    REMOVE_COMPLETED_TODOS,
    SET_STATE
} from '../types/todos'

export const addTodo = (value: string): TodoAction => ({ type: ADD_TODO, value });
export const removeTodo = (id: number): TodoAction => ({ type: REMOVE_TODO, id });
export const editTodo = (id: number, value: string): TodoAction => ({ type: CHANGE_TODO, id, value });
export const toggleTodo = (id: number): TodoAction => ({ type: TOGGLE_TODO, id});
export const toggleAllTodos = (completed: boolean): TodoAction => ({ type: TOGGLE_ALL_TODOS, completed});
export const removeCompletedTodos = (): TodoAction => ({ type: REMOVE_COMPLETED_TODOS});
export const setState = (todos: Todos): TodoAction => ({ type: SET_STATE, todos });