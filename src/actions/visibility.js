// @flow

import {
    CHANGE_VISIBILITY_FILTER
} from '../types/visibility'

import type {
    Filter,
    ChangeFilter
} from '../types/visibility'

export const changeFilter = (filter: Filter) : ChangeFilter => ({type: CHANGE_VISIBILITY_FILTER, filter});