// @flow

import idb from 'idb';
import type { Todo, Todos, Id } from '../types/todos'

const TODOS_MAIN : string = 'todo-store';
const TODOS_STORE : string = 'todos';

const dbPromise = idb.open(TODOS_MAIN, 1, upgradeDB => {
    upgradeDB.createObjectStore(TODOS_STORE, {
        keyPath: 'id'
    });
});

class IdbTodos {
  get(key : string) : Promise<Todo> {
    return dbPromise.then(db => {
      return db.transaction(TODOS_STORE)
        .objectStore(TODOS_STORE).get(key);
    });
  }

  getAll() : Promise<Todos> {
      return dbPromise.then(db => {
          return db.transaction(TODOS_STORE)
              .objectStore(TODOS_STORE).getAll()
      })
  }

  set(val : Todo) : Promise<void> {
    return dbPromise.then(db => {
      const tx = db.transaction(TODOS_STORE, 'readwrite');
      tx.objectStore(TODOS_STORE).put(val);
      return tx.complete;
    });
  }

  delete(key : Id) : Promise<void> {
    return dbPromise.then(db => {
      const tx = db.transaction(TODOS_STORE, 'readwrite');
      tx.objectStore(TODOS_STORE).delete(key);
      return tx.complete;
    });
  }

  deleteCompleted() : Promise<void> {
    return this.getAll().then(todos => {
      todos.forEach(todo => {
        if (todo.completed) {
          this.delete(todo.id)
        }
      })
    })
  }
}

export default new IdbTodos()
  